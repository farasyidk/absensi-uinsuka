//
//  AjukanViewController.swift
//  UINSunanKalijaga
//
//  Created by Maz Rizqi on 12/11/18.
//  Copyright © 2018 Joskoding. All rights reserved.
//

import UIKit

class AjukanViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnIzin: UIButton!
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(getDatas), for: .valueChanged)
        
        return refreshControl
    }()
    var datas: [dataIjin]? = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.getDatas()
        
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresher
        } else {
            collectionView.addSubview(refresher)
        }
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (datas?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "cell"
        var stts: String
        let celll = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! dataCollectionCollectionViewCell
        
        celll.keterangan.text = "Fadqurrosyidik ijin \(datas?[indexPath.row].keterangan ?? "ss") waktu \(datas?[indexPath.row].tgl_ijin ?? "ss")"
        print(datas?[indexPath.row].status ?? "3")
        if Int(datas?[indexPath.row].status ?? "3") == 0 {
            stts = "proses"
        } else if Int(datas?[indexPath.row].status ?? "3") == 1 {
            stts = "diterima"
        } else {
            stts = "ditolak"
        }
        celll.status.text = stts
        
        celll.btnDownload.addTarget(self, action: #selector(self.getPdf), for: .touchUpInside)
        
        return celll
    }

    @objc func getPdf() {
        let stringUrl = "https://enos.itcollege.ee/~jpoial/allalaadimised/reading/Android-Programming-Cookbook.pdf"
        let url = URL(string: stringUrl)!
        // set up the session
        let fileName = String((url.lastPathComponent)) as NSString
        // Create destination URL
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: stringUrl)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    do {
                        //Show UIActivityViewController to save the downloaded file
                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                        for indexx in 0..<contents.count {
                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                                let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                self.present(activityViewController, animated: true, completion: nil)
                            }
                        }
                    }
                    catch (let err) {
                        print("error: \(err)")
                    }
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            } else {
                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
    }
    
    @objc func getDatas() {
        let urlApi : String = "https://01farasyid.000webhostapp.com/welcome/ijin"
        let configuration = URLSessionConfiguration.default
        let headers: [NSObject : AnyObject] = ["Accept" as NSObject:"application/json" as AnyObject];
        configuration.httpAdditionalHeaders = headers;
        let session = URLSession(configuration: configuration)
        let dataTasking = session.dataTask(with: URL(string: urlApi)! as URL) { (data : Data?, response : URLResponse?, error : Error?) in
            if let httpResponse = response as? HTTPURLResponse {
                print("err \(httpResponse.statusCode)")
            }
            if error == nil{
                do{
                    
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    
                    if let datasFromJsons = jsonData["data"] as? [[String: AnyObject]] {
                        print("hey \(String(describing: datasFromJsons[0]["id_karyawan"]))")
                        for dataFromJson in datasFromJsons {
                            let ijin = dataIjin()
                            
                            ijin.id = dataFromJson["id"] as? Int
                            ijin.id_karyawan = dataFromJson["id_karyawan"] as? String
                            ijin.id_pemberi = dataFromJson["id_pemberi"] as? String
                            ijin.tgl_ijin = dataFromJson["tgl_ijin"] as? String
                            ijin.tgl_masuk = dataFromJson["tgl_masuk"] as? String
                            ijin.keterangan = dataFromJson["keterangan"] as? String
                            ijin.status = dataFromJson["status"] as? String
                            
                            print("cek \(ijin)")
                            self.datas?.append(ijin)

                        }
                    }
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
                catch{
                        print("Terjadi Kesalahan Get Data")
                    }
                }
            }
            dataTasking.resume()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        debugPrint("Download finished: \(location)")
        do {
            let downloadedData = try Data(contentsOf: location)
            
            DispatchQueue.main.async(execute: {
                print("transfer completion OK!")
                
                let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
                let destinationPath = documentDirectoryPath.appendingPathComponent("swift.pdf")
                
                let pdfFileURL = URL(fileURLWithPath: destinationPath)
                FileManager.default.createFile(atPath: pdfFileURL.path,
                                               contents: downloadedData,
                                               attributes: nil)
                
                if FileManager.default.fileExists(atPath: pdfFileURL.path) {
                    print("pdfFileURL present!") // Confirm that the file is here!
                }
            })
        } catch {
            print(error.localizedDescription)
        }
    }
    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }

    
}



