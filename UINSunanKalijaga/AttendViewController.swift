//
//  AttendViewController.swift
//  UINSunanKalijaga
//
//  Created by MacBook on 10/28/16.
//  Copyright © 2016 Joskoding. All rights reserved.
//

import UIKit

class AttendViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imagePickerController : UIImagePickerController!
    let defaults = UserDefaults.standard
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var attenBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attenBtn.setTitle("Absen Sekarang", for: UIControlState.normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func attendNow(_ sender: AnyObject) {
        if myImageView.image != nil {
            uploadRequest()
        }else{
            //ambil dari galery
//            let myPickerController = UIImagePickerController()
//            myPickerController.delegate = self
//            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
//            self.present(myPickerController, animated: true, completion: nil)
            
            /* Untuk Kamera */
            /*
             if UIImagePickerController.isSourceTypeAvailable(.camera){
             imagePicker =  UIImagePickerController()
             imagePicker.delegate = self
             imagePicker.sourceType = .camera
             present(imagePicker, animated: true, completion: nil)
             }else{
             print("Camera tidak Tersedia")
             }
             */
            
            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .camera
            present(imagePickerController, animated: true, completion: nil)
            
            // Change Button Title
            self.attenBtn.setTitle("Tag!", for: UIControlState.normal)
        }
    }
    func uploadRequest(){
        let myUrl = NSURL(string: "https://01farasyid.000webhostapp.com/welcome/uppic")
        let request = NSMutableURLRequest(url: myUrl as! URL)
        request.httpMethod = "POST"
        let p_nip = self.defaults.string(forKey: "nip")
        print("NIPnya berapa ? \(p_nip)")
        let param = [
            "nip" : "\(p_nip)"
        ]
        let boundary = generateBoundaryString()
        print("Hasil Boundary \(boundary)")
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let imageData = UIImageJPEGRepresentation(myImageView.image!, 0.1)
        print("Apakah Ada Image : \(imageData)")
        if imageData == nil{
            return
        }
        request.httpBody = createBodyWithParameter(parameters: param, filePathKey: "photo", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        // Upload Indicator
        self.attenBtn.isEnabled = false
        self.attenBtn.setTitle("Processing . . .", for: UIControlState.normal)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data : Data?, response : URLResponse?, error : Error?) in
            if error != nil {
                print("Terjadi Error \(error)")
                return
            }
            print("***Response Object****** \(response)")
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("***Response Data**** \(responseString)")
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                print("****Response JSON****\(json)")
                if let parseJSON = json{
                    let status = parseJSON["status"] as? String
                    print("Berhasil : \(status)")
                    if status == "success" {
                        let directory:String = parseJSON["directory"] as! String
                        let createdat:String = String(describing: Date())
                        let lat = self.defaults.string(forKey: "latitude")
                        let lng = self.defaults.string(forKey: "longitude")
                        self.setAttend(nip: p_nip!, lat: lat!, lng: lng!, photo: directory, macad: "FE:00:00:01", createdat: createdat)
                    }
                }
            }catch{
                print("Terjadi Error 2 \(error)")
            }
        }
        task.resume()
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        myImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    func createBodyWithParameter(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData()
        if parameters != nil{
            for(key, value) in parameters!{
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "_yyyyMMdd_HHmmss"
        let strDate = dateFormater.string(from: Date())
        let nip:String? = self.defaults.string(forKey: "nip")
//        let filename = "attendnip2.jpg"
        let filename = "\(nip ?? "0")\(strDate).jpg"
        print(filename)
        let mymetype = "image/jpg"
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mymetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    func setAttend(nip : String, lat : String, lng : String, photo : String, macad : String, createdat : String){
        let url = URL(string: "https://01farasyid.000webhostapp.com/welcome/absen")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        // Parameter
        let params = "nip=\(nip)&latitude=\(lat)&longitude=1\(lng)&photo=\(photo)&macaddress=\(macad)&created_at=\(createdat)";
        request.httpBody = params.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                print("Terjadi Error \(error)")
                return
            }
            
            print("Responses :\(response) ")
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                if let parseJSON = json{
                    let status = parseJSON["status"] as? String
                    if status == "success" {
                        self.attenBtn.isEnabled = true
                        self.attenBtn.setTitle("Absen Sukses", for: UIControlState.normal)
                        self.myImageView.image = nil
                        print("Apakah Berhasil Absen ? : \(status ?? "gagal")")
                    }
                }
            }catch{
                print(error)
            }
        }
        task.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NSMutableData{
    func appendString(string : String){
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
