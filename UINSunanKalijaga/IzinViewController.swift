//
//  IzinViewController.swift
//  UINSunanKalijaga
//
//  Created by Maz Rizqi on 16/12/18.
//  Copyright © 2018 Joskoding. All rights reserved.
//

import UIKit

class IzinViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var edtKeterangan: UITextField!
    @IBOutlet weak var edtDates: UITextField!
    @IBOutlet weak var edtDatee: UITextField!
    @IBOutlet weak var btnIzin: UIButton!
    @IBOutlet weak var myImageView: UIImageView!
    
    private var datePickers: UIDatePicker?
    private var datePickere: UIDatePicker?
    let dateFormatter = DateFormatter()
    var imagePickerController : UIImagePickerController!
//    var myImageView: UIImageView!
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        datePickers = UIDatePicker()
        datePickers?.datePickerMode = .date
        datePickers?.addTarget(self, action: #selector(self.dateChanged(datePickers:)), for: .valueChanged)
        
        datePickere = UIDatePicker()
        datePickere?.datePickerMode = .date
        datePickere?.addTarget(self, action: #selector(self.dateChangede(datePickere:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IzinViewController.viewTapped(gesturRecognizer:)))

        view.addGestureRecognizer(tapGesture)
        edtDates.inputView = datePickers
        edtDatee.inputView = datePickere
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        
    }
    
    func viewTapped(gesturRecognizer: UIGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePickers: UIDatePicker) {
        edtDates.text = dateFormatter.string(from: datePickers.date)
        view.endEditing(true)
    }
    
    @objc func dateChangede(datePickere: UIDatePicker) {
        edtDatee.text = dateFormatter.string(from: datePickere.date)
        view.endEditing(true)
    }
    
    @IBAction func fotoNow(_ sender: AnyObject) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .savedPhotosAlbum;
        myPickerController.allowsEditing = false
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    @IBAction func izinNow(_ sender: Any) {
        uploadRequest()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
           myImageView.image = image
        } else {
            print("hmm error")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadRequest(){
        let myUrl = NSURL(string: "https://01farasyid.000webhostapp.com/welcome/uppic")
        let request = NSMutableURLRequest(url: myUrl as! URL)
        request.httpMethod = "POST"
        let p_nip = self.defaults.string(forKey: "nip")
        print("NIPnya berapa ? \(p_nip)")
        let param = [
            "nip" : "\(p_nip)"
        ]
        let boundary = generateBoundaryString()
        print("Hasil Boundary \(boundary)")
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let imageData = UIImageJPEGRepresentation(myImageView.image!, 0.1)
        print("Apakah Ada Image : \(imageData)")
        if imageData == nil{
            return
        }
        request.httpBody = createBodyWithParameter(parameters: param, filePathKey: "photo", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        // Upload Indicator
        self.btnIzin.isEnabled = false
        self.btnIzin.setTitle("Processing . . .", for: UIControlState.normal)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data : Data?, response : URLResponse?, error : Error?) in
            if error != nil {
                print("Terjadi Error \(error)")
                return
            }
            print("***Response Object****** \(response)")
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("***Response Data**** \(responseString)")
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                print("****Response JSON****\(json)")
                if let parseJSON = json{
                    let status = parseJSON["status"] as? String
                    print("Berhasil : \(status)")
                    if status == "success" {
                        let directory:String = parseJSON["directory"] as! String
                        let createdat:String = String(describing: Date())
                        let lat = self.defaults.string(forKey: "latitude")
                        let lng = self.defaults.string(forKey: "longitude")
                        self.setAttend(nip: p_nip!, tgs: self.edtDates.text ?? "", tge: self.edtDatee.text ?? "", photo: directory, ket: self.edtKeterangan.text ?? "")
                    }
                }
            }catch{
                print("Terjadi Error 2 \(error)")
            }
        }
        task.resume()
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createBodyWithParameter(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData()
        if parameters != nil{
            for(key, value) in parameters!{
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "_yyyyMMdd_HHmmss"
        let strDate = dateFormater.string(from: Date())
        let nip:String? = self.defaults.string(forKey: "nip")
        //        let filename = "attendnip2.jpg"
        let filename = "\(nip ?? "0")\(strDate).jpg"
        print(filename)
        let mymetype = "image/jpg"
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mymetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func setAttend(nip : String, tgs : String, tge : String, photo : String, ket : String){
        let url = URL(string: "https://01farasyid.000webhostapp.com/welcome/set_ijin")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        // Parameter
        let params = "nip=\(nip)&tgs=\(tgs)&tge=\(tge)&bukti=\(photo)&ket=\(ket)";
        request.httpBody = params.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                print("Terjadi Error \(error)")
                return
            }
            
            print("Responses :\(response) ")
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                if let parseJSON = json{
                    let status = parseJSON["status"] as? String
                    if status == "success" {
                        self.btnIzin.isEnabled = true
                        self.btnIzin.setTitle("Izin Sukses", for: UIControlState.normal)
                        self.myImageView.image = nil
                        print("Apakah Berhasil Absen ? : \(status ?? "gagal")")
                    }
                }
            }catch{
                print(error)
            }
        }
        task.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
