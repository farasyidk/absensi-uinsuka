//
//  RecentViewController.swift
//  UINSunanKalijaga
//
//  Created by MacBook on 10/28/16.
//  Copyright © 2016 Joskoding. All rights reserved.
//

import UIKit

class RecentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tagFirstDate: UIDatePicker!
    @IBOutlet weak var mobileTable: UITableView!
    @IBOutlet weak var fingerTable: UITableView!
    @IBOutlet weak var latestUpdateLabel: UILabel!
    let defaults = UserDefaults.standard
    // Untuk mobile
    @IBOutlet weak var loadBtn: UIButton!
    var m_nip:[String] = []
    var m_lat:[String] = []
    var m_lng:[String] = []
    var m_photo:[String] = []
    var m_macaddress:[String] = []
    var m_createdat:[String] = []
    
    // Untuk Finger
    var f_nip:[String] = []
    var f_tagdate:[String] = []
    var f_fingerip:[String] = []
    
    // Untuk Latest Update
    var latestUpdate:String = ""
    // Untuk Tanggal
    var tgs:String = ""
    var nip:String = ""
    let date = Date()
    let formatter = DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        nip = defaults.string(forKey: "nip") ?? "0"
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        getAbsen(nip: nip, tgs: "2016-01-01", tge: result)
        reloadAbsen()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func loadAttend(_ sender: AnyObject) {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormater.string(from: tagFirstDate.date)
        print("get tanggal \(strDate)")
        let nip = defaults.string(forKey: "nip")
        getAbsen(nip: nip!, tgs: strDate, tge: strDate)
        reloadAbsen()
    }
    func reloadAbsen(){
        self.mobileTable.delegate=self
        self.mobileTable.dataSource=self
        self.fingerTable.delegate=self
        self.fingerTable.dataSource=self
    }
    func getAbsen(nip:String, tgs:String, tge:String){
        // Indicator Loading
        self.loadBtn.setTitle("Loading . . .", for: UIControlState.normal)
        self.loadBtn.isEnabled = false
        
        //let urlApi = "http://api.kalau.web.id/api/absen?nip=\(nip)&tgs=\(tgs)&tge=\(tge)"
        let urlApi = "https://01farasyid.000webhostapp.com/welcome/get_absen/\(nip)/\(tgs)/\(tge)"
        let configuration = URLSessionConfiguration.default
        let headers: [NSObject : AnyObject] = ["Accept" as NSObject:"application/json" as AnyObject];
        configuration.httpAdditionalHeaders = headers;
        let session = URLSession(configuration: configuration)
        let dataTasking = session.dataTask(with: URL(string: urlApi)! as URL) { (data : Data?, response : URLResponse?, error : Error?) in
            if error == nil{
                do{
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                
//                    let presensiDictionary = jsonData["presensi"] as? NSDictionary
                    
//                    let latest_update = presensiDictionary?["latest_update"] as! String
                    let latest_update = jsonData["latest_update"] as! String
                    print(jsonData["latest_update"])
                    self.latestUpdateLabel.text = "terakhir diupdate \(latest_update)"
//                    let fingers = presensiDictionary!["data_absensi_finger"] as? [NSDictionary]
                    // Removing Data
                    self.f_nip.removeAll()
                    self.f_tagdate.removeAll()
                    self.f_fingerip.removeAll()
                    self.m_lat.removeAll()
                    self.m_lng.removeAll()
                    self.m_nip.removeAll()
                    self.m_photo.removeAll()
                    self.m_createdat.removeAll()
                    self.m_macaddress.removeAll()
//
//                    for finger in fingers! {
//                        let finger_ip = finger["finger_ip"] as! String
//                        print("Finger IP : \(finger_ip)")
//                        self.f_fingerip.append(finger_ip)
//                        let tag_date = finger["tag_date"] as! String
//                        print("Finger Tag Date : \(tag_date)")
//                        self.f_tagdate.append(tag_date)
//                        let finger_nip = finger["nip"] as! String
//                        print("Finger NIP : \(finger_nip)")
//                        self.f_nip.append(finger_nip)
//                    }
                    
//                    let mobiles = presensiDictionary!["data_absensi_mobile"] as? [NSDictionary]
                    let mobiles = jsonData["presensi"] as? [NSDictionary]
                    for mobile in mobiles! {
                        let latitude = mobile["latitude"] as! String
                        print("Latitude : \(latitude)")
                        self.m_lat.append(latitude)
                        let longitude = mobile["longitude"] as! String
                        print("Longitude : \(longitude)")
                        self.m_lng.append(longitude)
                        let mobile_nip=mobile["nip"] as! String
                        print("NIP : \(mobile_nip)")
                        self.m_nip.append(mobile_nip)
                        let photo = mobile["photo"] as! String
                        print("Link Photo : \(photo)")
                        self.m_photo.append(photo)
                        let macadd = mobile["macaddress"] as! String
                        print("Mac Address : \(macadd)")
                        self.m_macaddress.append(macadd)
                        let created = mobile["created_at"] as! String
                        print("Pada Tanggal : \(created)")
                        self.m_createdat.append(created)
                    }
                    
                    // Indicator Loading - Success
                    self.loadBtn.setTitle("Load Data", for: UIControlState.normal)
                    self.loadBtn.isEnabled = true
                    
                    print("Berhasil Reloaded")
                    self.mobileTable.reloadData()
                    self.fingerTable.reloadData()
                }
                catch{
                    print("Terjadi Kesalahan Get Data")
                }
            }
        }
        dataTasking.resume()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mobileTable {
            return self.m_nip.count
        }else{
            return self.f_nip.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == mobileTable{
            let mobilecell:UITableViewCell = self.mobileTable.dequeueReusableCell(withIdentifier: "mobilecell")! as UITableViewCell
            mobilecell.textLabel?.text=self.m_createdat[indexPath.row]
            mobilecell.detailTextLabel?.text=self.m_macaddress[indexPath.row]
            return mobilecell
        }else{
            let fingercell:UITableViewCell = self.fingerTable.dequeueReusableCell(withIdentifier: "fingercell")! as UITableViewCell
            fingercell.textLabel?.text=self.f_fingerip[indexPath.row]
            fingercell.detailTextLabel?.text=self.f_tagdate[indexPath.row]
            return fingercell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == mobileTable{
            print(m_macaddress[indexPath.row])
        }else{
            print(f_fingerip[indexPath.row])
        }
    }
    @IBAction func selectTagFirst(_ sender: AnyObject) {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormater.string(from: tagFirstDate.date)
        self.tgs = strDate
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
