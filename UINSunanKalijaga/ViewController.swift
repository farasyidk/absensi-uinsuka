//
//  ViewController.swift
//  UINSunanKalijaga
//
//  Created by MacBook on 10/27/16.
//  Copyright © 2016 Joskoding. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
//    var url = URL(string:"http://ios.pdef.space/auth.php")
    var url = URL(string:"https://01farasyid.000webhostapp.com/welcome/auth")
    var statusLogin:Bool = false
    let defaults = UserDefaults.standard
    @IBOutlet weak var usernameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func loginButton(_ sender: AnyObject) {
        let username:String=usernameInput.text!
        let password:String=passwordInput.text!
        if username == "" {
            
            self.usernameInput.placeholder = "username harus diisi"
            self.usernameInput.isFocused
            return
        }
        if password == "" {
            
            self.passwordInput.placeholder = "password harus diisi"
            self.passwordInput.isFocused
            return
        }
        authLogin(username: username, password: password)
        if statusLogin{
            self.performSegue(withIdentifier: "showdashboard", sender: nil)
        }else{
            print("Belum Login")
        }
        
    }
    func authLogin(username: String, password:String){
        print("auth")
        var request = URLRequest(url: self.url!)
        request.httpMethod = "POST"
        let params = "username=\(username)&password=\(password)"
        request.httpBody = params.data(using: String.Encoding.utf8)
        // Login Indicator
        self.loginIndicator.startAnimating()
        self.usernameInput.isEnabled = false
        self.passwordInput.isEnabled = false
        self.loginBtn.isEnabled = false
        self.loginBtn.setTitle("Loading . . .", for: UIControlState.normal)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                print("errorrr")
                self.statusLogin = false
                // Login Indicator
                self.loginIndicator.stopAnimating()
                self.loginBtn.isEnabled = true
                self.usernameInput.isEnabled = true
                self.passwordInput.isEnabled = true
                self.usernameInput.text = ""
                self.passwordInput.text = ""
                self.loginBtn.setTitle("Gagal, Coba Lagi", for: UIControlState.normal)
                return
            }
            do {
                if let httpResponse = response as? HTTPURLResponse {
                    print("error \(httpResponse.statusCode)")
                }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                print("data \(json)")
                let statusAuth = json["status"] as? Bool
                print("Hasil \(statusAuth)")
                if statusAuth!{
                    if let parseJSON = json["data"] as? [[String: AnyObject]] {
                        print("auth tru")
                        // Login Indicator
                        self.loginIndicator.stopAnimating()
                        self.loginBtn.isEnabled = true
                        self.loginBtn.setTitle("Tekan Untuk Lanjutkan", for: UIControlState.normal)
                        self.statusLogin = true
//                        let dataDictionary = parseJSON["data"] as? NSDictionary
                        // Default Data
                        print(parseJSON[0]["nip"] as! String)
                        let nip:String = parseJSON[0]["nip"] as! String
                        let fullname:String = parseJSON[0]["nama"] as! String
                        let level:String = parseJSON[0]["level"] as! String
                        self.defaults.set(nip, forKey: "nip")
                        self.defaults.set(fullname, forKey: "fullname")
                        self.defaults.set(level, forKey: "level")
                        self.defaults.synchronize()
                    }else{
                        print("auth fal")
                        self.statusLogin = false
                        // Login Indicator
                        self.loginIndicator.stopAnimating()
                        self.loginBtn.isEnabled = true
                        self.usernameInput.isEnabled = true
                        self.passwordInput.isEnabled = true
                        self.usernameInput.text = ""
                        self.passwordInput.text = ""
                        self.loginBtn.setTitle("Gagal, Coba Lagi", for: UIControlState.normal)
                    }

                } else {
                    self.statusLogin = false
                    // Login Indicator
                    self.loginIndicator.stopAnimating()
                    self.loginBtn.isEnabled = true
                    self.usernameInput.isEnabled = true
                    self.passwordInput.isEnabled = true
                    self.usernameInput.text = ""
                    self.loginBtn.setTitle("User atau pas salah, Coba Lagi", for: UIControlState.normal)
                }
            }catch{
                print("er \(error)")
                print("Tidak ada koneksi")
                // Login Indicator
                self.loginIndicator.stopAnimating()
                self.loginBtn.setTitle("Tidak Ada Koneksi, Coba Lagi", for: UIControlState.normal)
            }
        }
        task.resume()
    }
}

